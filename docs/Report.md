# COMP261 T12021 - Assignment 1 - The Journey Planner Network
 
## Minimum

* *Construct classes to represent* ***Stops***, ***Trips***, and ***Connections***
between two stops. The class should have methods to read the data from the files
and construct the objects.

See files for classes `Stop`, `Trip`, and `TripConnection`

* *Make methods to read, parse, and create your data structures.*

See `TripPlanner.onLoad(...)` function.

* *Draw the graph by filling in the `redraw` method. This method should call a
`draw()` method on `Connection` and `Stop`, and use the passed `Graphics` object.*

See `TripPlanner.redraw(Graphics g)`, `Connection.draw(Graphics g, ...)`,
`Stop.draw(Graphics g, ...)`

## Core

* *Allow the user to navigate the map, i.e. implement panning and zooming with
the buttons. When the buttons get pressed `onMove` called, and passed an enum
with the movement in it.*

See `TripPlanner.onMove(Move m)`

* *Make the program respond to the mouse so that the user can select a stop with
the mouse, and the program will then highlight it, and print out the name of the
stop and the id of all the trips going through the stop. This can be implemented
using `onClick`, using the passed `MouseEvent` object.*

See `TripPlanner.onClick(MouseEvent e)`

This function took me a fair bit of time to do. This is because when I was
writing the original `Stop.draw(...)` and `TripConnection.draw()` methods,
I incorrectly used the `Location` objects associated with the stops,
resulting in a flipped graph. I had *fixed* this, however it was causing
issues with translating the mouse location as well.

* *Implement the behavior of the search box in the top right, which should
allow a user to select a `Stop` by entering its name exactly, using the
`onSearch()` method*

See commit
[2f49bcd5](https://gitlab.com/katalysatorn/COMP261-T12021-A1/-/commit/2f49bcd5114e69b3994b2c24c6c3de6135fd22fc)
as this changes when we update it to use a `Trie` for the search.