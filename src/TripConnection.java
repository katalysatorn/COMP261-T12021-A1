import java.awt.*;

/**
 * Represents a connection between two Stops
 */
public class TripConnection {
    protected final Stop start;
    protected final Stop end;

    /**
     * Creates a new Connection Object
     *
     * @param start Beginning of connection
     * @param end   End of connection
     */
    public TripConnection(Stop start, Stop end) {
        this.start = start;
        this.end = end;
    }


    /**
     * Draws the TripConnection
     * <p>
     * This function also relies on TripPlanner.LINE_COLOR
     *
     * @param g      Graphics object to draw with
     * @param origin Origin of graph
     * @param scale  Scale of graph
     */
    public void draw(Graphics g, Location origin, double scale) {
        Point p1 = start.location.asPoint(origin, scale);
        Point p2 = end.location.asPoint(origin, scale);

        g.setColor(TripPlanner.LINE_COLOR);
        g.drawLine(
                p1.x,
                p1.y,
                p2.x,
                p2.y
        );
    }
}
