import java.util.ArrayList;

/**
 * Trip
 * <p>
 * This class represents a single Trip as defined in trips.txt (Example file)
 */
public class Trip {
    protected final String tripID;
    protected final ArrayList<Stop> stopSequence;

    /**
     * Creates a new trip without any stops.
     *
     * @param tripID The ID of the trip (trip_id)
     */
    public Trip(String tripID) {
        this.tripID = tripID;
        this.stopSequence = new ArrayList<>();
    }

    /**
     * Creates a new trip with the provided list of stops (stop_sequence)
     *
     * @param tripID       The ID of the trip (trip_id)
     * @param stopSequence The list of stopSequence (stop_sequence)
     */
    public Trip(String tripID, ArrayList<Stop> stopSequence) {
        this.tripID = tripID;
        this.stopSequence = stopSequence;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (Stop s : stopSequence) {
            sb.append("[" + s.stopID + "] ");
            sb.append(s.stopName + " ");
        }

        return sb.toString();
    }
}
