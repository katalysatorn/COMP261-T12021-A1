import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Represents a Trie object.
 * <p>
 * Contains a char value as well as an java.util.ArrayList of children which are also Trie objects.
 * <p>
 * Useful for creating an easily searchable dictionary.
 */
public class Trie {
    public final char value;

    public final ArrayList<Trie> children;

    /**
     * Initialises the Trie with a value and creates an empty list of children
     *
     * @param value
     */
    public Trie(char value) {
        this.value = value;
        this.children = new ArrayList<>();
    }

    /**
     * Creates a Trie populated with the String provided
     *
     * @param str To make Trie from
     */
    public void add(String str) {
        // If string is empty then we are at the end of the recursive loop
        if (str.isEmpty()) {
            return;
        }

        char next = str.charAt(0);

        for (Trie t : children) {
            // If child already exists, then we can just add the next part of the string to it
            if (t.value == next) {
                t.add(str.substring(1));
                return;
            }
        }

        // otherwise need to create the letter too
        Trie newTrie = new Trie(next);
        newTrie.add(str.substring(1));
        children.add(newTrie);
    }

    /**
     * Finds and returns the list of children of a Trie at the end of the String str.
     *
     * @param str to find
     * @return
     */
    public Trie find(String str) {
        if (children.isEmpty()) {
            return this;
        }

        for (Trie t : children) {
            if (t.value == str.charAt(0)) {
                if (str.length() == 1) {
                    return t;
                } else {
                    return t.find(str.substring(1));
                }
            }
        }

        return this;
    }

    public ArrayList<String> getAllSubstrings(String prefix) {
        ArrayList<String> list = new ArrayList<>();

        for (Trie child : children) {
            if (child.isEndNode()) {
                list.add(prefix + child.value);
            } else {
                list.addAll(child.getAllSubstrings(prefix + child.value));
            }
        }

        return list;
    }

    /**
     * Returns true if this node has no more children.
     * @return
     */
    public boolean isEndNode() {
        return children.size() == 0;
    }

    /**
     * @return if the value of this try node is root or not (value == ' ' && children.size() == 0)
     */
    public boolean isRoot() {
        return (value == ' ') && children.size() == 0;
    }
}
