import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Stop
 * <p>
 * This class represents a single stop as defined by each row in stops.txt (Example file)
 */
public class Stop {
    protected final String stopID;
    protected final String stopName;
    protected final Location location;

    protected final ArrayList<TripConnection> connections;
    protected final ArrayList<Trip> trips;

    private boolean highlighted = false;

    /**
     * Allows the creation of an immutable Stop object
     *
     * @param stopID   The ID of the stop (stop_id)
     * @param stopName The name of the stop (stop_name)
     * @param location The location of the stop
     */
    public Stop(String stopID, String stopName, Location location) {
        this.stopID = stopID;
        this.stopName = stopName;
        this.location = location;

        this.connections = new ArrayList<>();
        this.trips = new ArrayList<>();
    }

    /**
     * Draws the stop
     * <p>
     * This function also relies on TripPlanner.POINT_COLOR, TripPlanner.POINT_SIZE, and TripPlanner.HIGHLIGHT_COLOR
     *
     * @param g      Graphics object to draw with
     * @param origin Origin of graph
     * @param scale  Scale of graph
     */
    public void draw(Graphics g, Location origin, double scale) {
        if (!highlighted) {
            g.setColor(TripPlanner.POINT_COLOR);
        } else {
            g.setColor(TripPlanner.HIGHLIGHT_COLOR);
        }

        Point p = this.location.asPoint(origin, scale);

        g.fillOval(
                p.x,
                p.y,
                TripPlanner.POINT_SIZE,
                TripPlanner.POINT_SIZE
        );
    }

    /**
     * Prints the stop info in the format:
     * [stopID] stopName
     * This stop has the following trips:
     * trip1
     * trip2
     * ...
     *
     * @param ta Textarea to print to.
     */
    public void printStopInfo(JTextArea ta) {
        ta.setText("");

        ta.append("\n" + toString());

        ta.append("\nThis stop has the following trips:");
        for (Trip t : trips) {
            ta.append("\n" + t.toString());
        }
    }

    /**
     * Sets whether this stop is highlighted or not.
     * @param highlight Highlighted T|F
     */
    public void setHighlight(boolean highlight) {
        highlighted = highlight;
    }

    @Override
    public String toString() {
        return "[" + stopID + "] " + stopName + " " + location.toString();
    }
}
