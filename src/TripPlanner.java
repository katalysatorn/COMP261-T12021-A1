import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class TripPlanner extends GUI {
    public static final int CLOSE_DIST = 20;
    public static final int TRANSLATE_DIST = 5;
    public static final int ZOOM_STEP = 1;

    public static final int POINT_SIZE = 5;
    public static final Color LINE_COLOR = Color.LIGHT_GRAY;
    public static final Color POINT_COLOR = Color.BLUE;
    public static final Color HIGHLIGHT_COLOR = Color.YELLOW;

    private static double scale = 10;
    private static Location origin = new Location(-20, 20);

    private final HashMap<String, Stop> allStopsByID = new HashMap<>();
    private final HashMap<String, Stop> allStopsByName = new HashMap<>();
    private final HashMap<String, Trip> allTrips = new HashMap<>();
    private final ArrayList<TripConnection> allTripConnections = new ArrayList<>();

    private final Trie rootTrie = new Trie(' ');

    @Override
    protected void redraw(Graphics g) {
        for (TripConnection conn : allTripConnections) {
            conn.draw(g, origin, scale);
        }

        // The stops must be rendered after otherwise they will not be visible.
        for (Stop stop : allStopsByID.values()) {
            stop.draw(g, origin, scale);
        }
    }

    /**
     * This function:
     * 1. Works out where on the canvas the mouse has clicked
     * 2. Iterates over the stops and keep track of the closest one
     * 3. Finds if the closest stop is in the range of the mouse click
     * y. Highlight the stop
     * <p>
     * 4. Exits
     */
    @Override
    protected void onClick(MouseEvent e) {
        System.out.println(e.getPoint());
        Location mouseLoc = Location.newFromPoint(e.getPoint(), origin, scale);

        getTextOutputArea().setText(mouseLoc.toString());

        Stop closest = null;
        double closeDist = CLOSE_DIST; // This can be CLOSE_DIST or Double.MAX_VALUE, but I chose CLOSE_DIST as it is lower

        for (Stop s : allStopsByID.values()) {
            s.setHighlight(false);

            if (s.location.distanceTo(mouseLoc) < closeDist) {
                System.out.println("Found new closest stop: " + s.toString());
                closest = s;
                closeDist = s.location.distanceTo(mouseLoc);
            }
        }

        if (closest != null) {
            closest.setHighlight(true);
            closest.printStopInfo(getTextOutputArea());
        }
    }

    @Override
    protected void onSearch() {
        // Index all stops if root is empty
        if (rootTrie.children.size() == 0) {
            for (Stop s : allStopsByID.values()) {
                rootTrie.add(s.stopName);
            }
        }

        String searchQuery = getSearchBox().getText();

        Trie searchTrie = rootTrie.find(searchQuery);

        ArrayList<String> searchResults = searchTrie.getAllSubstrings(searchQuery);

        ArrayList<Stop> found = new ArrayList<>();

        for (Stop s : allStopsByName.values()) {
            s.setHighlight(false);

            if (searchResults.contains(s.stopName)) {
                s.setHighlight(true);
                found.add(s);
            }
        }

        getTextOutputArea().setText("Found " + found.size() + " stops:");

        for (Stop s : found) {
            getTextOutputArea().append("\n[" + s.stopID + "] " + s.stopName);
        }
    }

    @Override
    protected void onMove(Move m) {
        switch (m) {
            case NORTH:
                origin = origin.moveBy(0, TRANSLATE_DIST);
                break;
            case SOUTH:
                origin = origin.moveBy(0, -TRANSLATE_DIST);
                break;
            case EAST:
                origin = origin.moveBy(TRANSLATE_DIST, 0);
                break;
            case WEST:
                origin = origin.moveBy(-TRANSLATE_DIST, 0);
                break;
            case ZOOM_IN:
                scale += ZOOM_STEP;
                break;
            case ZOOM_OUT:
                scale -= ZOOM_STEP;
                break;
        }
    }

    @Override
    protected void onLoad(File stopFile, File tripFile) {
        // First the stops
        try (BufferedReader br = new BufferedReader(new FileReader(stopFile))) {

            br.readLine(); // 1. We can ignore the first line

            // 2. We split each lines by tabs (\t)

            String ln = br.readLine();
            String[] spl;

            while (ln != null) {
                spl = ln.split("\t");
                Stop s = new Stop(
                        spl[0], // stop_id
                        spl[1], // stop_name
                        Location.newFromLatLon(
                                Double.parseDouble(spl[2]), // stop_lat
                                Double.parseDouble(spl[3]) // stop_lon
                        )
                );

                // 3. This creates the stop and places it in allStops so it can be managed
                allStopsByID.put(spl[0], s);
                // 4. Also create all of them but accessible via name for searching later.
                allStopsByName.put(spl[1], s);

                ln = br.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Next we can do the Trips
        try (BufferedReader br = new BufferedReader(new FileReader(tripFile))) {

            br.readLine(); // 1. Same with stops, we can ignore the first line

            // 2. Split the lines by tabs again
            String ln = br.readLine();
            String[] spl;
            ArrayList<Stop> stopSequence;

            while (ln != null) {
                stopSequence = new ArrayList<>();
                spl = ln.split("\t");

                int j;
                TripConnection conn;

                // 3. Create the stop sequence and place it into allTrips
                // This part also adds the connections for the Stops too
                for (int i = 1; i < spl.length; i++) {
                    stopSequence.add(allStopsByID.get(spl[i]));

                    j = i + 1;

                    if (j < spl.length) {
                        conn = new TripConnection(
                                allStopsByID.get(spl[i]),
                                allStopsByID.get(spl[j])
                        );

                        if (!allStopsByID.get(spl[i]).connections.contains(conn)) {
                            allStopsByID.get(spl[i]).connections.add(conn);

                            allTripConnections.add(conn);
                        }
                    }
                }

                // 4. Add the stop sequence and trip stuff to Trip and add it to allTrips

                Trip t = new Trip(spl[0], stopSequence);
                allTrips.put(spl[0], t);

                for (Stop s : stopSequence) {
                    s.trips.add(t);
                }

                ln = br.readLine();
            }

            System.out.println(allTrips);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Runs the program
     *
     * @param a Program variables. Does not accept any, so will be ignored.
     */
    public static void main(String[] a) {
        new TripPlanner();
    }
}
