import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TrieTest {
    @Test
    public void TrieTest() {
        Trie test = new Trie(' ');

        test.add("bomb");
        test.add("boom");

        Trie list = test.find("bo");

        assert list.children.size() == 2;
    }

    @Test
    public void SubstringTest() {
        Trie test = new Trie(' ');

        test.add("bomb");
        test.add("boom");
        test.add("blood");

        Trie subTrie = test.find("bo");
        ArrayList<String> substrings = subTrie.getAllSubstrings("bo");

        assert substrings.size() == 2;
        assert test.find("bo").getAllSubstrings("bo").size() == 2;
        assert test.find("bl").getAllSubstrings("bl").size() == 1;
    }
}
